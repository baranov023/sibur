import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state    : {
    data     : Array,
    headers  : Array,
    stats    : Object,
    important: Array
  },
  mutations: {
    setData(state, d) {
      return state.data = d;
    },
    setHeaders(state, d) {
      return state.headers = d;
    },
    setStats(state, d) {
      return state.stats = d;
    },
    setImportance(state, d) {
      
      return state.important = d;
    }
  },
  actions  : {},
  getters  : {
    getAllData(state) {
      return () => state.data;
    },
    getHeaders(state) {
      return () => state.headers;
    }
  }
});
